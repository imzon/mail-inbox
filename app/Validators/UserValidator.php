<?php

namespace App\Validators;

use \Prettus\Validator\LaravelValidator;

/**
 * Class UserValidator.
 *
 * @package namespace App\Validators;
 */
class UserValidator extends LaravelValidator
{
    const RULE_LOGIN = 'login';

    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        self::RULE_CREATE => [],
        self::RULE_UPDATE => [],
        self::RULE_LOGIN => [
            'email' => 'required',
            'password' => 'required'
        ]
    ];
}

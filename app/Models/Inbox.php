<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inbox extends Model
{
    use HasFactory;

    protected $fillable = ['account_id', 'folder', 'subject', 'body', 'uid', 'is_read', 'mail_at'];
}

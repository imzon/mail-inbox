<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    use HasFactory;

    protected $fillable = ['type', 'username', 'password', 'recovery', 'is_read', 'is_active', 'last_id'];

    protected $appends =  ['has_inbox'];

    public function inboxes()
    {
        return $this->hasMany(Inbox::class)->orderByDesc('uid');
    }

    public function getHasInboxAttribute()
    {
        return !!$this->inboxes()->where('is_read', 0)->count();
    }
}

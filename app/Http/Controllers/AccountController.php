<?php

namespace App\Http\Controllers;

use App\Criteria\AccountAccessibleCriteria;
use App\Http\Controllers\Controller;
use App\Repositories\InboxRepository;
use Illuminate\Http\Request;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Repositories\AccountRepository;

/**
 * Class AccountsController.
 *
 * @package namespace App\Http\Controllers;
 */
class AccountController extends Controller
{
    /**
     * @var AccountRepository
     */
    protected $repository;
    /**
     * @var InboxRepository
     */
    private $inboxRepository;


    /**
     * AccountsController constructor.
     *
     * @param AccountRepository $repository
     */
    public function __construct(AccountRepository $repository, InboxRepository $inboxRepository)
    {
        $this->repository = $repository;
        $this->inboxRepository = $inboxRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accounts = $this->repository->paginate();

        return view('accounts.index', compact('accounts'));
    }

    /**
     * Create the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('accounts.create_edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     *
     */
    public function store(Request $request)
    {
        try {
            $account = $this->repository->create($request->all());
            $response = [
                'message' => 'Account created.',
                'data' => $account->toArray(),
            ];
            return redirect()->route('accounts.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {
            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $account = $this->repository->with('inboxes')->find($id);
        $inboxes = $this->inboxRepository
            ->pushCriteria(new AccountAccessibleCriteria($id))
            ->orderBy('uid', 'desc')
            ->paginate();
        return view('accounts.show', compact('account', 'inboxes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $account = $this->repository->find($id);

        return view('accounts.create_edit', compact('account'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $account = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Account updated.',
                'data' => $account->toArray(),
            ];

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->update(['status' => -1], $id);

        return redirect()->back()->with('message', 'Account deleted.');
    }

    public function inbox($id, $uid)
    {
        $account = $this->repository->with('inboxes')->find($id);
        $inbox = $this->inboxRepository
            ->pushCriteria(new AccountAccessibleCriteria($id))
            ->find($uid);
        $inbox->is_read = 1;
        $inbox->save();
        return view('accounts.inbox', compact('account', 'inbox'));
    }
}

<?php


namespace App\Exceptions;

use Throwable;

class MethodNotAllowedException extends \Exception
{
    public function __construct()
    {
        parent::__construct(trans('error.method_not_allowed'), 405);
    }
}

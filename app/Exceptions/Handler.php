<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use LogicException;
use Prettus\Validator\Exceptions\ValidatorException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthenticationException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (Throwable $e) {
            if (request()->expectsJson() || request()->is('api/*')) {
                return $this->exHandler($e);
            }
        });
    }

    public function exHandler(Throwable $exception)
    {
        $message = $exception->getMessage();
        $type = class_basename($exception);
        $errors = [];

        switch (true) {
            case $exception instanceof ValidatorException:
                $httpCode = 400;
                $message = trans('error.validation_exception');
                $errors = [];
                foreach ($exception->getMessageBag()->toArray() as $key => $value) {
                    $errors[$key] = $value[0];
                }
                break;
            case $exception instanceof ValidationException:
                $httpCode = 400;
                $errors = $exception->errors();
                break;
            case $exception instanceof LogicException:
                $httpCode = 400;
                $message = trans('error.logic_exception');
                break;
            case $exception instanceof AuthenticationException:
                $httpCode = 401;
                break;
            case $exception instanceof AuthorizationException:
                $httpCode = 403;
                break;
            case $exception instanceof NotFoundHttpException:
            case $exception instanceof ModelNotFoundException:
                $httpCode = 404;
                $message = trans('error.record_not_found');
                break;
            default:
                $httpCode = 500;
                break;
        }

        $response = [
            'code' => $exception->getCode() ?: $httpCode,
            'message' => $message,
            'type' => $type,
        ];

        if (count($errors)) {
            $response['errors'] = $errors;
        }

        if (env('APP_DEBUG')) {
            $response['file'] = $exception->getFile();
            $response['line'] = $exception->getLine();
        }

        return response()->json(['error' => $response], $httpCode);
    }
}

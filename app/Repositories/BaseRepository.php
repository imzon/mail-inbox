<?php

namespace App\Repositories;

use App\Models\User;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository as IBaseRepository;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class UserRepositoryRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
abstract class BaseRepository extends IBaseRepository
{
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function validate($rule, array $attributes, $id = null)
    {
        $this->resetValidator();

        if ($id) {
            $this->validator->setId($id);
        }
        $this->validator->with($attributes)->passesOrFail($rule);

        return $this;
    }

    public function getUser($hasOrFail = true)
    {
        $user = auth()->user();
        if ($hasOrFail &&  !$user instanceof User) {
            throw new \LogicException(trans('error.user_invalid'));
        }

        return $user;
    }

    public function skipValidator()
    {
        $this->validator = null;

        return $this;
    }

    /**
     * @throws RepositoryException
     */
    public function resetValidator()
    {
        if (is_null($this->validator)) {
            $this->makeValidator($this->validator);
        }
    }
}

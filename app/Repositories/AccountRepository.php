<?php

namespace App\Repositories;

use App\Models\Account;
use App\Validators\AccountValidator;
use Carbon\Carbon;
use Webklex\PHPIMAP\ClientManager;

/**
 * Class AccountRepositoryRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class AccountRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Account::class;
    }

    public function validator()
    {
        return AccountValidator::class;
    }

    public function processInbox(Account $account)
    {
        $cm = new ClientManager();
        $client = $cm->make([
            'host' => 'outlook.office365.com',
            'port' => 993,
            'encryption' => 'ssl',
            'validate_cert' => true,
            'username' => $account->username,
            'password' => $account->password,
            'protocol' => 'imap',
        ]);
        $client->connect();

        //Get all Mailboxes
        /** @var \Webklex\PHPIMAP\Support\FolderCollection $folders */
        $folders = $client->getFolders();
        $data = [];
        //Loop through every Mailbox
        /** @var \Webklex\PHPIMAP\Folder $folder */
        foreach ($folders as $folder) {
            //Get all Messages of the current Mailbox $folder
            /** @var \Webklex\PHPIMAP\Support\MessageCollection $messages */
            $folderName = $folder->name;
            $messages = $folder->messages()->unseen()->get();

            /** @var \Webklex\PHPIMAP\Message $message */
            foreach ($messages as $message) {
                try {
                    $item = [
                        'folder' => $folderName,
                        'uid' => $message->getUid(),
                        'subject' => $message->getSubject()->toString(),
                        'body' => $message->getHTMLBody(),
                        'mail_at' => Carbon::parse($message->getDate()),
                    ];
                    $data[] = $item;
                    $account->inboxes()->updateOrCreate([
                        'uid' => $message->getUid(),
                    ], $item);
                } catch (\Exception $e) {
                }
            }
        }
        return $data;
    }
}

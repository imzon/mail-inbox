<?php

namespace App\Repositories;

use App\Validators\UserValidator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\MessageBag;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class UserRepositoryRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class AuthRepository extends UserRepository
{
    public function logout()
    {
        $user = $this->getUser();
        $user->tokens()->delete();

        return true;
    }
    /**
     * @param array $attributes
     * @return array
     * @throws ValidatorException
     */
    public function login(array $attributes)
    {
        $this->validate(UserValidator::RULE_LOGIN, $attributes);

        $user = $this->model->where('email', $attributes['email'])->first();

        if (!$user || !Hash::check($attributes['password'], $user->password)) {
            throw new ValidatorException(new MessageBag([
                'email' => trans('error.invalid_credentials'),
            ]));
        }

        $currentToken = $user->createToken('api');

        return [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'token_type' => 'Bearer',
            'access_token' => $currentToken->plainTextToken,
        ];
    }
}

<?php

namespace App\Repositories;

use App\Models\Inbox;

/**
 * Class InboxRepositoryRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class InboxRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Inbox::class;
    }
}

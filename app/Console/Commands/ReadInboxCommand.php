<?php

namespace App\Console\Commands;

use App\Models\Account;
use App\Repositories\AccountRepository;
use Illuminate\Console\Command;

class ReadInboxCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inbox:read';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    /**
     * @var AccountRepository
     */
    private $accountRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(AccountRepository $accountRepository)
    {
        parent::__construct();
        $this->accountRepository = $accountRepository;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Account::query()->chunk(50, function ($accounts) {
            foreach ($accounts as $account) {
                $this->info('Processing account ' . $account->username);
                $data = $this->accountRepository->processInbox($account);
                $this->info('Total scanned ' . count($data));
            }
        });
        return 0;
    }
}

<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;

/**
 * Class UserRequestCriteriaCriteria.
 *
 * @package namespace App\Criteria;
 */
abstract class BaseCriteria implements CriteriaInterface
{
    /**
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    public function __construct()
    {
        $this->request = request();
    }
}

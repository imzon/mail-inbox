<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 *
 * @package namespace App\Criteria;
 */
class AccountAccessibleCriteria extends BaseCriteria
{
    private $accountId;

    public function __construct($accountId)
    {
        $this->accountId = $accountId;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('account_id', $this->accountId);
    }
}

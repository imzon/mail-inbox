<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class UserRequestCriteriaCriteria.
 *
 * @package namespace App\Criteria;
 */
class UserRequestCriteria extends BaseCriteria
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ($search = $this->request->get('search')) {
            $model = $model->where(function ($query) use ($search) {
               return $query->where('username', 'like', "%$search%")
                   ->orWhere('email', 'like', "%$search%")
                   ->orWhere('phone_number', 'like', "%$search%");
            });
        }

        if ($type = $this->request->get('type')) {
            $model = $model->where('type', $type);
        }

        return $model;
    }
}

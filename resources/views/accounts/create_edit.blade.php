<x-app-layout>
    <x-slot name="header">
        <div class="lg:flex lg:items-center lg:justify-between">
            <div class="flex-1 min-w-0">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    Create New User
                </h2>
            </div>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />

                    @if(isset($account))
                        <form method="POST" action="{{ route('accounts.update', [$account->id]) }}">
                        @method('PUT')
                    @else
                        <form method="POST" action="{{ route('accounts.store') }}">
                    @endif

                    @csrf
                        <div>
                            <x-label for="username" value="Username" />
                            <x-input id="username" class="w-full" type="text" name="username" :value="old('username', isset($account) ? $account->username : null)"/>
                        </div>
                        <div>
                            <x-label for="password" value="Password" />
                            <x-input id="password" class="w-full" type="text" name="password" :value="old('password', isset($account) ? $account->password : null)"/>
                        </div>
                        <div class="mt-4">
                            <x-label for="is_active" value="Status" />
                            <x-select id="is_active" name="is_active" :options="[1 => 'Enable', 0 => 'Disable']" :value="old('is_active', isset($account) ? $account->is_active : 1)"></x-select>
                        </div>
                        <div class="flex items-center justify-end mt-4">
                            <a href="{{ route('accounts.index') }}">
                                <x-button type="button" class="text-white bg-indigo-600">
                                    Hủy
                                </x-button>
                            </a>
                            <x-button class="ml-3">
                                {{ isset($account) ? 'Cập Nhật' : 'Thêm Mới' }}
                            </x-button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

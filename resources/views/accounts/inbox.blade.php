<x-app-layout>
    <x-slot name="header">
        <div class="lg:flex lg:items-center lg:justify-between">
            <div class="flex-1 min-w-0">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    Inbox
                </h2>
            </div>
            <div class="mt-5 flex lg:mt-0 lg:ml-4">
                <x-link href="{{ route('accounts.show', $account->id) }}">
                    Back
                </x-link>
            </div>
        </div>
    </x-slot>

    <div class="py-5">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white shadow overflow-hidden sm:rounded-lg">
                <div class="px-4 py-5 sm:px-6">
                    <h3 class="text-lg leading-6 font-medium text-gray-900">
                        {{ $inbox->subject }}
                    </h3>
                </div>
                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg table-responsive">
                    {!! $inbox->body !!}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

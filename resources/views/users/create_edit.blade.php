<x-app-layout>
    <x-slot name="header">
        <div class="lg:flex lg:items-center lg:justify-between">
            <div class="flex-1 min-w-0">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    Create New User
                </h2>
            </div>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />

                    @if(isset($user))
                        <form method="POST" action="{{ route('admin.users.update', [$user->id]) }}">
                        @method('PUT')
                    @else
                        <form method="POST" action="{{ route('admin.users.store') }}">
                    @endif

                    @csrf
                    <!-- Email Address -->
                        <div>
                            <x-label for="name" value="Name" />
                            <x-input id="name" type="text" name="name" :value="old('name', isset($user) ? $user->name : null)"/>
                        </div>
                        <!-- Email Address -->
                        <div class="mt-4">
                            <x-label for="email" :value="__('Email')" />

                            <x-input id="email" type="email" name="email" :value="old('email', isset($user) ? $user->email : null)" required :disabled="isset($user)"/>
                        </div>
                        <!-- Password -->
                        <div class="mt-4">
                            <x-label for="password" :value="__('Password')" />
                            <x-input id="password"
                                     type="password"
                                     name="password"/>
                        </div>

                        <!-- Confirm Password -->
                        <div class="mt-4">
                            <x-label for="password_confirmation" :value="__('Confirm Password')" />
                            <x-input id="password_confirmation"
                                     type="password"
                                     name="password_confirmation"/>
                        </div>
                        <div class="mt-4">
                            <x-label for="is_active" value="Status" />
                            <x-select id="is_active" name="is_active" :options="[1 => 'Enable', 0 => 'Disable']" :value="old('is_active', isset($user) ? $user->is_active : 1)"></x-select>
                        </div>
                        <div class="flex items-center justify-end mt-4">
                            <a href="{{ route('admin.users.index') }}">
                                <x-button type="button" class="text-white bg-indigo-600">
                                    Hủy
                                </x-button>
                            </a>
                            <x-button class="ml-3">
                                {{ isset($user) ? 'Cập Nhật' : 'Thêm Mới' }}
                            </x-button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

<?php

return [
    'user_invalid' => 'The user invalid',
    'validation_exception' => 'Error validating your request data.',
];

<?php


namespace Database\Seeders;


use Illuminate\Support\Facades\DB;

trait QueryTableTrait
{
    protected $primaryKey = 'id';

    protected function insertRecords($table, $array, $ignoreExists)
    {
        foreach ($array as $attributes) {
            $query = DB::table($table);
            $isExists = $query->where($this->primaryKey, $attributes[$this->primaryKey])->count();
            if (!$isExists) {
                $query->insert($attributes);
            } elseif (!$ignoreExists) {
                $query->update($attributes);
            }
        }
    }

    public function replaceRecords($table, $array)
    {
        $this->insertRecords($table, $array, false);
    }

    public function insertIgnoreRecords($table, $array)
    {
        $this->insertRecords($table, $array, true);
    }
}
